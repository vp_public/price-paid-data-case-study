########################################
# Importing relevant libraries
########################################
import pandas as pd
import numpy as np
import os
import glob
from datetime import datetime


########################################
#Bulk Import the Price Paid Raw Data into a DataFrame
########################################
"""
From past experience, using numpy to append the individual dataframes improves runtimes, so that is why I am using np.vstack() instead of pd.concat()
While working on this task I made use of the data between 2012 and 2020 (all corresponding *.csv files provided in the link)
I kept the raw data column names as they were, for consistency.
"""
path = r".\RawData"
allFiles = glob.glob(os.path.join(path,"*.csv"))


np_array_list = []
for f in allFiles:
    df = pd.read_csv(f,index_col=None, header=None)
    np_array_list.append(df.to_numpy())

combined_np_array = np.vstack(np_array_list)
all_PP_data = pd.DataFrame(combined_np_array)

all_PP_data.columns = ["Transaction unique identifier","Price","Date of Transfer","Postcode","Property Type","Old/New","Duration"
                     ,"PAON","SAON","Street","Locality","Town/City","District","County","PPD Category Type","Record Status - monthly file only"]

# Creating 3 samples of the DataFrame created above in order to test the subsequent functions in a timely manner
small_AA_df = all_PP_data.sample(n = 10, random_state = 1) # only 10 records so I can quickly visualize transformations if needed
medium_AA_df = all_PP_data.sample(n = 10000, random_state = 2)
large_AA_df = all_PP_data.sample(n = 1000000, random_state = 3)


########################################
#1. Most expensive houses by county
########################################
"""
Order the records in the DataFrame by price descending and then remove all duplicated across the 'County' column. 
This will keep the records with the biggest price for each distinct 'County'. 
"""
def objective1(df):
    # make sure the datatypes of the columns used within this function are as intended
    df = df.astype({"Price": 'float64', "County": 'string'})
    
    return df.sort_values('Price', ascending=False).drop_duplicates(['County'])

"""
A different approach or improvement would be to use df.groupby() in order to group the records by specific columns and do other transformations afterwards
I preferred using my approach in this case due to the simplicity of the transformation, and the readability of the code.
"""


########################################
#2. Top 5 districts by quarterly transaction value
########################################
"""
For this task my plan can be split in the following steps:
1. Create all the necessary derived columns (Year, Month, Quarter, Postcode_District)
2. Group by the relevant columns and produce the output

Note: I am assuming that we are interested in a breakdown by Year and Quarter, not just Quarter. 
      If we were to only look into a breakdown by Quarter, if the dataset would be from 2018 Q4 and the whole 2019
      , then the Q4 breakdown would include data from 2 years, while Q1-3 will have data from 2019 only.
""" 
    
def objective2(df):
    # make sure the datatypes of the columns used within this function are as intended
    df = df.astype({"Price": 'float64', "Postcode": 'string',"Date of Transfer": 'datetime64'})
    
    # We should keep the rows that don't have empty Postcodes, since we are interested in top Districts by first half of postcode
    df = df[df['Postcode'].notna()]
    
    """
    Trim the unnecessary part of the postcode, i.e. keep the first half (up the the blank space)
    The strategy was to locate the whitespace between the 2 halves and keep the first half only
    """
    df['Postcode_District'] = df.apply(lambda row: row.Postcode[:str(row.Postcode).strip().find(" ")], axis = 1)

    
    """
    Add a column for month and one for year, both derived from 'Date of Transfer'
    """   
    df['Month'] = df['Date of Transfer'].dt.month
    df['Year'] = df['Date of Transfer'].dt.year
    
    """
    The next transformation is to add a column with the quarter in which an individual transaction took place.
    Since Q1 = Months 1-3, Q2 = Months 4-6, Q3 = Months 7-9, Q4 = Months 10-12,
    the simple maths formula of 'floor((Month - 1)/3 + 1)' will give the desired output
    """
    df['Quarter'] = (df['Month'] - 1)//3 + 1
    
    """
    Now that we have all the derived columns we need, we are ready to create the output for the second task. 
    Start by grouping on the relevant columns, 'Year', 'Quarter', 'Postcode_District' and suming over the 'Price' Column
    """
    grouped_df = df.groupby(['Year','Quarter','Postcode_District'])['Price'].sum().reset_index()
    result = grouped_df.sort_values('Price',ascending = False).groupby(['Year','Quarter']).head(5)
    
    #adding the 'Year/Quarter' column below to use it as an index for the output
    result['Year/Quarter'] = result['Year'].astype(str) + ' Q' + result['Quarter'].astype(str)
    return result.sort_values(['Year','Quarter'],ascending = True).set_index('Year/Quarter')


########################################
# 3. Transaction Value Concentration
########################################
"""
For this task my plan can be split in the following steps:
1. Create a custom function that takes in a pandas Series with 'Price' values and calculates the transaction value concentration
2. Create the main function where we will group by 'Year' and 'Property Type' and apply the aggregating function created at step 1
3. Inside the function at step 2, use the .pivot method to obtain the final output
"""
def tranvalcon(s):
    # make sure the series is numeric 
    s = pd.to_numeric(s, errors='coerce')
    
    # order values in descending order
    s = s.sort_values(ascending = False)
    
    # calculate the total transaction value in the series, the 80% treshold we are interested in and the total number of transactions(records)
    total = s.sum()
    treshold = 4*total/5
    total_count=s.size
    
    # Make use of 'cumsum' to calculate the cumulative sum of the values in the series
    cumulative_s = s.cumsum().reset_index(drop = True) # resetting index because the new index (plus 1) will equal the number of values in the cumulative sum
    result = (cumulative_s[cumulative_s >= treshold].index[0] + 1)/total_count
    
    return result
    

def objective3(df):
    # make sure the datatypes of the columns used within this function are as intended
    df = df.astype({"Price": 'float64', "Property Type": 'string',"Date of Transfer": 'datetime64'})
    
    """
    Add a column for month and one for year, both derived from 'Date of Transfer'
    """
    df['Month'] = df['Date of Transfer'].dt.month
    df['Year'] = df['Date of Transfer'].dt.year
    
    # group the DataFrame accordingly and aggregate using the custom function. Lastly, pivot it to get to the desired format
    grouped_df = df.groupby(['Year','Property Type'])['Price'].agg([tranvalcon]).reset_index()
    pivoted_df = grouped_df.pivot(index='Year', columns='Property Type', values='tranvalcon')
    return pivoted_df    


########################################
# 4. Volume & Median Price Comparisons
########################################
"""
For this task my plan can be split in the following steps:
1. Create a new 'Price_Bracket' column for both imputs by making use of a custom function
2. Group by 'Bracket' and calculate the number of transactions & median price (for both inputs)
3. Aggregate the 2 processed dataframes to produce the final result
"""
def pricebracket(x):
    # make sure the input is numeric 
    x = float(x)
    
    if x > 0 and x <= 250000:
        return (0,250000)
    elif x > 250000 and x <= 500000:
        return (250000,500000)
    elif x > 500000 and x <= 750000:
        return (500000,750000)
    elif x > 750000 and x <= 1000000:
        return (750000,1000000)
    elif x > 1000000 and x <= 2000000:
        return (1000000,2000000)   
    elif x > 2000000 and x <= 5000000:
        return (2000000,5000000)
    elif x > 5000000:
        """
        Since we want the Price_Bracket column to appear as a tuple, I will assume the upper limit for the price can be 
        taken as 1,000,000,000, especially since the maximum price recorded in the data set for years 2012-2020 is 594,300,000      
        
        I could have used 'inf' instead of a numeric value for this bracket, but I assumed that keeping everything to numeric values looks better.
        """
        return (5000000,1000000000) 
    return np.nan     
        
        
                
def objective4(df1, df2):
    # make sure the datatypes of the columns used within this function are as intended
    df1 = df1.astype({"Price": 'float64'})
    df2 = df2.astype({"Price": 'float64'})
    
    # use the pricebracket function to create the 'Price_Bracket' column in both dataframes
    df1['Price_Bracket'] = df1['Price'].apply(pricebracket)
    df2['Price_Bracket'] = df2['Price'].apply(pricebracket)
    
    #group by 'Price_Bracket and calculate the '
    grouped_df1 = df1.groupby(['Price_Bracket'])['Price'].agg(['count','median']).reset_index()
    grouped_df2 = df2.groupby(['Price_Bracket'])['Price'].agg(['count','median']).reset_index()
    
    """
    For the final step I am assuming that we would want all price brackets to be present in our output, 
    not just the ones that appear in the data.
    Secondly, I am assuming that the percentage change will be calculated using the formula (x2 - x1)/x1, the alternative being (x1 - x2)/x2
    """ 
    result = pd.DataFrame({'Price_Bracket': [(0,250000),(250000,500000),(500000,750000),(750000,1000000),(1000000,2000000),(2000000,5000000),(5000000,1000000000)],
                   'Percentage Change in Count':[np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan],
                   'Percentage Change in Median':[np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]})
    result = result.set_index('Price_Bracket')
    
    for index, row in result.iterrows():
        row['Percentage Change in Count'] = (grouped_df2[grouped_df2['Price_Bracket'] == index]['count'] - grouped_df1[grouped_df1['Price_Bracket'] == index]['count'])/grouped_df1[grouped_df1['Price_Bracket'] == index]['count']
        row['Percentage Change in Median'] = (grouped_df2[grouped_df2['Price_Bracket'] == index]['median'] - grouped_df1[grouped_df1['Price_Bracket'] == index]['median'])/grouped_df1[grouped_df1['Price_Bracket'] == index]['median']
    
    
    return result
    

########################################
# 5. Property returns - Bonus Objective   
########################################
"""
For this task my plan can be split in the following steps:
1. Construct 3 more columns that will store the Previous Price (Price from the previous transaction at that address), Previous Date and a Full Address column 
2. Use the columns above to calculate Holding Period and price change (where possible) 
3. Aggregate using groupby to obtain the result
"""
def objective5(df):
    # make sure the datatypes of the columns used within this function are as intended
    df = df.astype({"Price": 'float64',"Date of Transfer": 'datetime64'})
    
    # sorting transactions in chronological order
    df = df.sort_values('Date of Transfer', ascending = True)
    
    # Assume the 4 columns used below are enough to determine a unique address
    df[['Postcode','PAON','SAON','Street']] = df[['Postcode','PAON','SAON','Street']].fillna(' ')
    df['Full_Address'] = df['Postcode'] + ' | ' + df['PAON'] + ' | ' +df['SAON'] + ' | ' +df['Street']
    
    df['Previous_Price'] = df.groupby(['Full_Address'])['Price'].shift(1)
    df['Previous_Date_of_Transfer'] = df.groupby(['Full_Address'])['Date of Transfer'].shift(1)
    
    # getting ready to calculate the final output by introducing a few more calculated fields
    df['Price_New/Old'] = df['Price']/df['Previous_Price']
    df['Holding_Period'] = (df['Date of Transfer'] - df['Previous_Date_of_Transfer']).dt.days
    
    # convert the period into a fraction of a year
    df['Holding_Period_Year'] = (df['Date of Transfer'] - df['Previous_Date_of_Transfer']).dt.days/365 
    
    # Assume the formula for the annualised change is the following: (1+ ann_change)**years = New_Price / Old_Price
    df['Annualised_Change'] = df['Price_New/Old'] ** (1/df['Holding_Period_Year']) - 1
    
    df['Year'] = df['Date of Transfer'].dt.year
    result = df.groupby(['Year','Property Type']).agg({'Holding_Period': 'mean','Annualised_Change': 'mean'})
    return result
